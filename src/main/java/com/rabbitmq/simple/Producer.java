package com.rabbitmq.simple;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Producer {


    /**
     * 所有的中间件技术都是基于tcp/ip协议基础之上构建的新型协议规范，只不过rabbitmq遵循的是amqp
     * ip  port
     *
     * 1. 创建连接工程
     * 2. 创建连接Connection
     * 3. 通过连接获取通道Channel
     * 4. 通过创建交换机，申明队列，绑定关系，路由key，发送消息，接收消息
     * 5. 准备消息内容
     * 6. 发送消息给队列queue
     * 7. 关闭连接
     * 8. 关闭通道
     */

    public static void main(String[] args) {
        Connection connection = null;
        Channel channel = null;
        //创建连接工程
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("39.99.134.41");
        factory.setPort(5672);
        factory.setUsername("shen");
        factory.setPassword("123456");
        factory.setVirtualHost("/");
        //创建连接connection
        try {
            connection = factory.newConnection("生产者");
            //通过连接获取通道
            channel = connection.createChannel();
            String queueName = "queue1";
            /**
             * @params1 队列的名称
             * @params2 是否要持久化durable-false 所谓持久化消息是否存盘，如果false 非持久化 true 持久化
             * @params3 排他性，是否是独占独立
             * @params4 是否自动删除，随着最后一个消费者消息完毕以后是否把队列自动删除
             * @params5 携带附属参数
             *
             */
            channel.queueDeclare(queueName,false,false,false,null);
            //准备消息内容
            String message = "this is a demo ";
            //发送消息给队列
            /**
             * @params1 交换机
             * @params2 队列、路由key
             * @params3 消息的状态控制
             * @params4 消息主题
             *  虽然没有指定交换机但是一定会存在一个默认的交换机，不存在没有交换机的队列
             */
            channel.basicPublish("",queueName,null,message.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }finally {

            //关闭通道
            if (channel != null && channel.isOpen()){
                try {
                    channel.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //关闭连接
            if (connection != null && connection.isOpen()){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }


    }
}
