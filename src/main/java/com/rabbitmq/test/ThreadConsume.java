package com.rabbitmq.test;

import com.sun.org.apache.xpath.internal.operations.And;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ThreadConsume {

    public static void main(String[] args) {

        getOrder();
        /**
         * 订单内部处理，之后进行用户反馈
         */
        dealAndRes();

    }

    public static void getOrder(){
        System.out.printf("用户请求订单");
    }
    public static void  dealAndRes() {
        try {
            Thread.sleep(2000);
            System.out.println("订单处理已完成，等待反馈用户");
            //调用用户反馈模块
            resUser();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void resUser(){
        ExecutorService threadPool = Executors.newFixedThreadPool(3);
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"发送用户短信");
            }
        });
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"发送用户邮件");
            }
        });
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"发送用户微信");
            }
        });
    }
}
