package com.rabbitmq.work.lunxun;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Producer {



    public static void main(String[] args) {
        Connection connection = null;
        Channel channel = null;
        //创建连接工程
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("39.99.134.41");
        factory.setPort(5672);
        factory.setUsername("shen");
        factory.setPassword("123456");
        factory.setVirtualHost("/");
        //创建连接connection
        try {
            connection = factory.newConnection("生产者");
            //通过连接获取通道
            channel = connection.createChannel();
            //准备消息内容
            for (int i = 0; i < 20; i++) {
                String msg = "工作模式——轮询"+i;
                channel.basicPublish("","queue1",null,msg.getBytes());
            }

            System.out.println("消息已发送");
        } catch (Exception e) {
            e.printStackTrace();
        }finally {

            //关闭通道
            if (channel != null && channel.isOpen()){
                try {
                    channel.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //关闭连接
            if (connection != null && connection.isOpen()){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }


    }
}
