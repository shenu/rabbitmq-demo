package com.rabbitmq.work.lunxun;

import com.rabbitmq.client.*;

import java.io.IOException;

public class Work2 {

    public static void main(String[] args) {
        Connection connection = null;
        Channel channel = null;
        //创建连接工程
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("39.99.134.41");
        factory.setPort(5672);
        factory.setUsername("shen");
        factory.setPassword("123456");
        factory.setVirtualHost("/");
        //创建连接connection
        try {
            connection = factory.newConnection("消费者-Work1");
            //通过连接获取通道
            channel = connection.createChannel();
            Channel finalChannel = channel;
//            finalChannel.basicQos(1);
            finalChannel.basicConsume("queue1", true, new DeliverCallback() {
                @Override
                public void handle(String s, Delivery delivery) throws IOException {
                    System.out.println("Work-2收到的消息是："+new String(delivery.getBody(),"utf-8"));
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }, new CancelCallback() {
                @Override
                public void handle(String s) throws IOException {
                    System.out.println("消息接收失败");
                }
            });

            System.out.println("开始接收消息");
            System.in.read();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {

            //关闭通道
            if (channel != null && channel.isOpen()){
                try {
                    channel.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //关闭连接
            if (connection != null && connection.isOpen()){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }


    }

}
