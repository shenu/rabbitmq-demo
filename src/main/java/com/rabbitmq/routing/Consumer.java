package com.rabbitmq.routing;

import com.rabbitmq.client.*;

import java.io.IOException;

public class Consumer  {

    private static Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Connection connection = null;
            Channel channel = null;
            //创建连接工程
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("39.99.134.41");
            factory.setPort(5672);
            factory.setUsername("shen");
            factory.setPassword("123456");
            factory.setVirtualHost("/");
            //创建连接connection
            //获得队列名称
            final String queueName = Thread.currentThread().getName();
            try {
                connection = factory.newConnection("消费者");
                //通过连接获取通道
                channel = connection.createChannel();
                Channel finalChannel = channel;


                finalChannel.basicConsume(queueName, true, new DeliverCallback() {
                    @Override
                    public void handle(String s, Delivery message) throws IOException {
                        System.out.println(queueName+"收到的消息" + new String(message.getBody(), "utf-8"));
                    }
                }, new CancelCallback() {
                    @Override
                    public void handle(String s) throws IOException {
                        System.out.println("失败了");
                    }
                });
                System.in.read();
            } catch (Exception e) {
                e.printStackTrace();
            }finally {

                //关闭通道
                if (channel != null && channel.isOpen()){
                    try {
                        channel.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //关闭连接
                if (connection != null && connection.isOpen()){
                    try {
                        connection.close();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
    };


    public static void main(String[] args) {
        new Thread(runnable,"queue1").start();
        new Thread(runnable,"queue2").start();
        new Thread(runnable,"queue3").start();
        new Thread(runnable,"queue5").start();
    }
}
