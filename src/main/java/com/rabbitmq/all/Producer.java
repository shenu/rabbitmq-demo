package com.rabbitmq.all;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Producer {


    /**
     * 所有的中间件技术都是基于tcp/ip协议基础之上构建的新型协议规范，只不过rabbitmq遵循的是amqp
     * ip  port
     *
     * 1. 创建连接工程
     * 2. 创建连接Connection
     * 3. 通过连接获取通道Channel
     * 4. 通过创建交换机，申明队列，绑定关系，路由key，发送消息，接收消息
     * 5. 准备消息内容
     * 6. 发送消息给队列queue
     * 7. 关闭连接
     * 8. 关闭通道
     */

    public static void main(String[] args) {
        Connection connection = null;
        Channel channel = null;
        //创建连接工程
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("39.99.134.41");
        factory.setPort(5672);
        factory.setUsername("shen");
        factory.setPassword("123456");
        factory.setVirtualHost("/");
        //创建连接connection
        try {
            connection = factory.newConnection("生产者");
            //通过连接获取通道
            channel = connection.createChannel();
            //准备消息内容
            String message = "this is a demo ->全代码操作过程-路由模式 ";
            //准备交换机
            String exchangeName = "direct-message-exchange";
            //定义路由key
            //指定交换机的类型
            String type = "direct";
            /**
             * 如果你用界面把queue和exchange的关系先绑定的话，你代码就不需要再编写这些申明代码，可以让代码变得更加简洁，
             * 但是但是不容易读懂，
             */
            //申明注册交换机
            channel.exchangeDeclare(exchangeName,type,true);
            //申明队列
            channel.queueDeclare("queue5",true,false,false,null);
            channel.queueDeclare("queue6",true,false,false,null);
            channel.queueDeclare("queue7",true,false,false,null);
            //绑定队列
            channel.queueBind("queue5",exchangeName,"order");
            channel.queueBind("queue6",exchangeName,"user");
            channel.queueBind("queue7",exchangeName,"product");
            //发送消息给队列
            /**
             * @params1 交换机
             * @params2 队列、路由key
             * @params3 消息的状态控制
             * @params4 消息主题
             *  虽然没有指定交换机但是一定会存在一个默认的交换机，不存在没有交换机的队列
             */
            channel.basicPublish(exchangeName,"order",null,message.getBytes());
            System.out.println("消息已发送");
        } catch (Exception e) {
            e.printStackTrace();
        }finally {

            //关闭通道
            if (channel != null && channel.isOpen()){
                try {
                    channel.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //关闭连接
            if (connection != null && connection.isOpen()){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }


    }
}
