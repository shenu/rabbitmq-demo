package com.rabbitmq.topics;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Producer {



    public static void main(String[] args) {
        Connection connection = null;
        Channel channel = null;
        //创建连接工程
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("39.99.134.41");
        factory.setPort(5672);
        factory.setUsername("shen");
        factory.setPassword("123456");
        factory.setVirtualHost("/");
        //创建连接connection
        try {
            connection = factory.newConnection("生产者");
            //通过连接获取通道
            channel = connection.createChannel();
            //准备消息内容
            String message = "this is a demo ->主题模式 ";
            //准备交换机
            String exchangeName = "topic-EX";
            //定义路由key
            String routeKey = "com.aa.bb";
            //指定交换机的类型
            String type = "topic";



            //发送消息给队列
            /**
             * @params1 交换机
             * @params2 队列、路由key
             * @params3 消息的状态控制
             * @params4 消息主题
             *  虽然没有指定交换机但是一定会存在一个默认的交换机，不存在没有交换机的队列
             */
            channel.basicPublish(exchangeName,routeKey,null,message.getBytes());
            System.out.println("消息已发送");
        } catch (Exception e) {
            e.printStackTrace();
        }finally {

            //关闭通道
            if (channel != null && channel.isOpen()){
                try {
                    channel.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //关闭连接
            if (connection != null && connection.isOpen()){
                try {
                    connection.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }


    }
}
